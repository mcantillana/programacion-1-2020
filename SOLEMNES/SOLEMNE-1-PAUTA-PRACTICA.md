# SOLEMNE-1
Universidad Andrés Bello 

## Parte de desarrollo (Práctica)

### Preguntas

1. Escriba un Programa en Pseudocódigo (12 pts)

***"Analizador de edades"***

En la Universidad requieren analizar las edades de un conjunto de alumnos. Para ello le piden a usted que confeccione un algoritmo para tal fin. 
Este programa debe permitir ingresar n edades hasta que el operador del programa ingrese una edad con valor 0. Una vez ingresado el valor 0, el programa debe indicar lo siguiente por pantalla:

* Indicar la cantidad de personas analizadas
* La edad más longeva del grupo
* La edad más pequeña del grupo
* Cantidad de personas mayores a 20 años
* El promedio de edad del grupo analizado
* Cantidad de personas que tengan 24 años
* El promedio de personas con mayor a 25 años
* Cantidad de personas con edad par (e.g 20, 22, 24, 26, etc)

**SOLUCIÓN**

~~~
INICIO

// definicion de variables
edad entero;
contador entero;
mayor entero;
menor entero;
mayor_a_20 entero;
cantidad_edad_24 entero;
suma_edades entero;
promedio entero;
cantidad_mayor_a_25 entero;
suma_mayor_a_25 entero;
cantidad_edad_par entero;
promedio_mayor_25 entero;

// Inicialización de variables
contador = 0;
mayor_a_20 = 0;
suma_edades = 0;
cantidad_edad_24 = 0;
cantidad_mayor_a_25 = 0;
suma_mayor_a_25 = 0;
cantidad_edad_par = 0;
promedio_mayor_25 = 0;


HACER
    ESCRIBIR "Ingrese su edad";
    LEER edad;

    SI edad != 0 entonces

        // solo para la primera edad
        SI contador == 0 entonces
            mayor = edad;
            menor = edad;
        SINO
            SI edad > mayor entonces
                mayor = edad;
            FIN SI

            SI edad < menor entonces
                menor = edad;
            FIN SI 

        FIN SI

        // contar si edad es mayor a 20
        SI edad > 20 entonces
            mayor_a_20 = mayor_a_20 + 1;
        FIN SI

        // cantidad de personas que tengan 24
        SI edad == 24 entonces
            cantidad_edad_24 = cantidad_edad_24 + 1;
        FIN SI

        // para calcular promedio de mayores de 25
        SI edad > 25 entonces
            cantidad_mayor_a_25 = cantidad_mayor_a_25 + 1;
            suma_mayor_a_25 = suma_mayor_a_25 + edad;
        FIN SI

        SI edad MOD 2 == 0 entonces
            cantidad_edad_par = cantidad_edad_par + 1;
        FIN SI

        contador = contador + 1;
        suma_edades = suma_edades + edad;
    FIN SI

MIENTRAS (edad != 0)


ESCRIBIR "Cantidad de personas analizadas: ";
ESCRIBIR contador;

ESCRIBIR "La edad más longeva del grupo: ";
ESCRIBIR mayor;

ESCRIBIR "La edad más pequeña del grupo: ";
ESCRIBIR menor;

promedio = suma_edades / contador;
ESCRIBIR "El promedio de edades del grupo: ";
ESCRIBIR promedio;

ESCRIBIR "Cantidad de personas con 24 años: ";
ESCRIBIR cantidad_edad_24;

promedio_mayor_25 = suma_mayor_a_25 / cantidad_mayor_a_25;
ESCRIBIR "El promedio de edades mayor a 25 es: ";
ESCRIBIR promedio_mayor_25;

ESCRIBIR "Cantidad de personas con edades pares: ";
ESCRIBIR cantidad_edad_par;


FIN
~~~

2. Escriba y complete la Tabla de Seguimiento del problema anterior con al menos 8 edades de prueba. (12 pts)

**SOLUCIÓN**
* [Pauta en excel](tabla_seguimiento.xlsx)

3. Diseñe un algoritmo en Pseudocódigo que imprima la siguiente serie de fracciones para n terminos con signos alternos, donde n es ingresado por el usuario. (12 pts)

Por ejemplo si n = 5, se debe mostrar en pantalla las siguientes fracciones

<pre>
1/2 – 3/4 + 5/6 – 7/8 + 9/10
</pre>

y el resultado de la operación aritmética. Que para el caso de uso es 0,608333333.

**SOLUCIÓN**
~~~
INICIO

n entero;
i entero;
numerador entero;
denominador entero;
acumulacion entero;

numerador = 1;
denominador = 2;
acumulaicon = 0;

ESCRIBIR "Ingrese n";
LEER n;

DESDE (i=1;i<=n;i=i+1)
INICIO
    
    SI i MOD 2 == 0 entonces
        ESCRIBIR "-" numerador "/" denominador
        acumulaicon = acumulaicon - (numerador / denominador)
    SINO
        SI i == 1 entonces
            ESCRIBIR numerador "/" denominador
        SINO
            ESCRIBIR "+" numerador "/" denominador
        FIN SI
        acumulaicon = acumulaicon + (numerador / denominador)
    FIN SI

    numerador = numerador + 2;
    denominador = denominador + 2;
FIN

resultado = numerador / denominador ;
ESCRIBIR resultado

FIN
~~~
