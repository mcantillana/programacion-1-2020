# SOLEMNE-2
Universidad Andrés Bello 

## Reglas de Control:
<pre>
* Las respuestas deben ser enviadas al correo
* El Copy & Paste será penalizado
* Tiempo: 120 minutos.
* Puntaje Total 36 Puntos.
</pre>

## Ejercicio
### Descripción de la actividad
Estamos al final de semestre y necesitamos calcular algunos resultados necesarios para cerrar el semestre de forma correcta. 

El curso cuenta con 7 alumnos. El programa debe permitir solicitar por cada alumno: 
* El nombre del alumno (debe aceptar espacios)
* La nota de la primera solemne
* La nota de la segunda solemne
* La nota de la tarea final
* La cantidad de asistencias totales que ha tenido el alumno. 

Las notas y las asistencias deben ser registradas en vectores de datos y las nombres en vectores de cadenas como se muestra a continuación. 

![alt text](img/img_ejercicio_solemne.png "Ejemplo de como registrar notas, alumnos y asistencia")

Con la información recolectada en los diferentes vectores, el programa debe entregar la siguiente información:

* Nota final de cada alumno: La ponderación para determinar la nota final es Solemne1 -> 45%, Solemne2 -> 45% y TareaFinal -> 10%. Este promedio lo debe almacenar en un vector de promedios
* Promedio del curso: El promedio debe ser calculado a partir de la nota final de cada alumno
* El peor promedio: Debe ser determinado a partir de la nota final
* Listado de alumnos aprobados (Un alumno aprueba con nota final >= 4.0)
* Listado de Alumnos, con su respectivo promedio final, con asistencia mayor o igual al 50%


### Requisitos funcionales
* Las notas deben estar entre 1 y 7 (debe permitir decimales). 
* La asistencia debe estar entre 1 y 12 (son valores enteros), donde 12 equivale al 100% de asistencia. Para validar la asistencia, debe crear una función que llame EsAsistenciaValida que reciba la asistencia como parámetro y retorn 1 si es válida y 0 si NO es válida
* Para validar las notas debe implementar una función con el nombre EsNotaValida donde recibirá una nota por parámetro y deberá entregar 1 si es válida y 0 si no es válida
* No debe permitir nombres mayores a 40 caracteres
* Debe mostrar los promedios con solo 1 decimal
* Los calculos deben estar separados del proceso de recolección de información

## Condiciones de entrega
* Debe enviar el código fuente con el nombre solemne2.c
* Debe adjuntar el código fuente y enviar un correo al email mcantillana@portal53.cl 
* Asunto del correo: PROGRAMACION1-SOLEMNE2
* Fecha de entrega: 04 Junio 2020 a las 22:00 hrs

