# SOLEMNE-1
Universidad Andrés Bello 

## Parte de desarrollo (Práctica)
### Reglas de Control:
<pre>
* Las respuestas deben ser entregadas en este mismo formulario
* El Copy & Paste será penalizado
* Tiempo: 60 minutos.
* Puntaje Total 36 Puntos.
</pre>

### Preguntas

1. Escriba un Programa en Pseudocódigo (12 pts)

***"Analizador de edades"***

En la Universidad requieren analizar las edades de un conjunto de alumnos. Para ello le piden a usted que confeccione un algoritmo para tal fin. 
Este programa debe permitir ingresar n edades hasta que el operador del programa ingrese una edad con valor 0. Una vez ingresado el valor 0, el programa debe indicar lo siguiente por pantalla:

* Indicar la cantidad de personas analizadas
* La edad más longeva del grupo
* La edad más pequeña del grupo
* Cantidad de personas mayores a 20 años
* El promedio de edad del grupo analizado
* Cantidad de personas que tengan 24 años
* El promedio de personas con mayor a 25 años
* Cantidad de personas con edad par (e.g 20, 22, 24, 26, etc)


2. Escriba y complete la Tabla de Seguimiento del problema anterior con al menos 8 edades de prueba. (12 pts)


3. Diseñe un algoritmo en Pseudocódigo que imprima la siguiente serie de fracciones para n terminos con signos alternos, donde n es ingresado por el usuario. (12 pts)

Por ejemplo si n = 5, se debe mostrar en pantalla las siguientes fracciones

<pre>
1/2 – 3/4 + 5/6 – 7/8 + 9/10
</pre>

y el resultado de la operación aritmética. Que para el caso de uso es 0,608333333.


## Condiciones de entrega
* Debe un documento de texto con el desarrollo de cada ejercicio
* Debe adjuntar el documento y enviar un correo al email mcantillana@portal53.cl 
* Asunto del correo: PROGRAMACION1-SOLEMNE1
* Fecha de entrega: 23 Abril 2020 a las 21:00 hrs
