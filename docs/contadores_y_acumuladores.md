# Contadores y Acumuladores

## Contadores

EJ. Para el ciclo HACER MIENTRAS

Realice un programa que cuente del 1 al 4, utilizando un ciclo hacer mientras.
~~~
INICIO
numero entero;
numero = 5;

contador entero;
contador = 1;

HACER
    ECRIBIR contador; // mostrando como salida
    contador = contador + 1;
MIENTRAS ( contador < numero );

FIN
~~~

Resolvamos el mismo ejercicio anterior, pero utilizando un ciclo DESDE:

La estructura del ciclo DESDE es la siguiente:

<pre>
DESDE (cond inicio; cond termino; contador)
INICIO
    // instrucciones
FIN
</pre>

Ahora escribamos nuestro algoritmo:
~~~
INICIO

numero entero;
numero = 5;

DESDE (contador = 1; contador < numero; contador = contador + 1)
    INICIO
        ESCRIBIR contador;
    FIN

FIN
~~~


## Acumuladores

Los acumuladores son otra estructura que nos permite acumular (sumar, restar o alguna operación aritmetica). Para ejemplificar intentaremos resolver el siguiente algoritmo.

Debe implementar un algoritmo que permita sumar los primeros 5 números

e.g.
<pre>
1+2+3+4+5 => 15
</pre>

Para resolver este problema, utilizaremos en una primera instancia el ciclo hacer mientras

~~~
INICIO

numero entero;
contador entero;
acumulacion entero;

contador = 1;
numero = 5;
acumulacion = 0;


HACER 
    // 1 + 2 + 3 + 4 + 5 
    acumulacion = acumulacion + contador;
    contador = contador + 1;
MIENTRAS (contador <= numero);

ESCRIBIR acumulacion;

FIN
~~~

Ahora, el mismo problema lo resolveremos utilizando el ciclo DESDE.

~~~
INICIO

numero entero;
acumulacion entero;
numero = 5;
acumulacion = 0;

DESDE (contador = 1; contador <= numero; contador = contador + 1)
INICIO
    acumulacion = acumulacion + contador;
FIN

ESCRIBIR acumulacion;

FIN
~~~

