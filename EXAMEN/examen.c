#include <stdio.h>
#include <stdlib.h>

#define MAX 99
#define MAX_PRODUCTS 3
// #define MAX_PRODUCTS (sizeof(ProductCodes)/sizeof(ProductCodes[0]))

// Prototipos
int IsValidProduct(int codes[], int code);
int IsValidQty(int qty);

int main() {
    
    // Código  | Producto    | Precio
    // -------------------------------
    // 865123  | Mouse       | $15900
    // 897162  | Teclado     | $8900
    // 897123  | Impresoa    | $34900

    int ProductCodes [] = {865123, 897162, 897123};
    char ProductNames[][MAX] = {"Mouse", "Teclado", "Impresora"};
    float ProductPrices[] = {15900, 8900, 34900};


    float totalSales[MAX_PRODUCTS];
    float AcumTotalSales = 0;

    int saleCode, saleQty;
    int continuar;
    int i;


    printf("== PRODUCTOS DISPONIBLES == \n");
    printf("Código\t| Producto\t| Precio\n");
    printf("---------------------------------\n");
    for(i=0;i<MAX_PRODUCTS;i++) {
        printf("%d\t| %s \t| $%.2f\n", ProductCodes[i],ProductNames[i],ProductPrices[i]);
    }
    printf("---------------------------------\n");
    

    // init total sales with 0 amount
    for(i=0;i<MAX_PRODUCTS;i++) {
        totalSales[i] = 0;
    }


    printf("== REGISTRAS VENTAS == \n");
    do {

        do {

            printf("\nIngrese código de producto:");
            scanf("%d", &saleCode);

            if(!IsValidProduct(ProductCodes, saleCode)) {
                printf("¡El código de producto no es valido. Intente nuevamente!\n");
            }

        } while (!IsValidProduct(ProductCodes, saleCode));

        do {

            printf("\nIngrese la cantidad: ");
            scanf("%d", &saleQty);

            if(!IsValidQty(saleQty)) {
                printf("¡La cantidad excede el maximo permitido! Intente nuevamente!\n");
            }

        } while(!IsValidQty(saleQty));

        for (i=0;i<MAX_PRODUCTS;i++) {
            if(ProductCodes[i] == saleCode) {
                totalSales[i] += (ProductPrices[i]*saleQty);
                AcumTotalSales += (ProductPrices[i]*saleQty);
            }
        }
        printf("¿Desea agregar otro producto (1=si, 0=no)?");
        scanf("%d", &continuar);

    } while (continuar);

    // Código  | Producto  | Ventas    | IVA (19%) |   sub-total
    // ----------------------------------------------------------
    // 865123  | Mouse     | $100000   | $19000    | $119000
    // 897162  | Teclado   | $350000   | $66500    | $416500
    // 897123  | Impresora | $532000   | $101080   | $633080
    // ----------------------------------------------------------
    // TOTAL: $1168580
    
    system("clear");
    printf("== REPORTE VENTAS == \n");
    printf("Código\t| Producto\t| Ventas\t| IVA (19%%)\t| sub-total\n");
    printf("---------------------------------------------------------------\n");
    for(i=0;i<MAX_PRODUCTS;i++) {
        printf("%d \t| %s \t| $%.2f \t| $%.2f \t| $%.2f\n", ProductCodes[i], ProductNames[i], totalSales[i], totalSales[i]*0.19, totalSales[i]*1.19);
    }
    printf("---------------------------------------------------------------\n");
    printf("TOTAL: %.2f + IVA\n", AcumTotalSales);

    return 0;
}


int IsValidProduct(int codes[], int code) {

    int i;
    for(i=0;i<MAX_PRODUCTS;i++) {
        if (codes[i] == code) {
            return 1;
        }
    }
    
    return 0;
}

int IsValidQty(int qty) {
    return (qty > 99)?0:1;
}