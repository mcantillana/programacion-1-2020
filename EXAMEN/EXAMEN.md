# EXAMEN
***Universidad Andrés Bello***
Asignatura: Programación I
Profesor: Miguel Cantillana

## Reglas de Control:
<pre>

* El Copy & Paste será penalizado
* Tiempo: 120 minutos.
* Puntaje Total 36 Puntos.

</pre>

## Descripción del problema
Una compañia manufacturera comercializa 3 tipos de productos diferentes en el mercado. Se pide que usted desarrolle un programa que permita registrar las ventas de cada producto y entregar un reporte de ventas con el resumen de compras.

Los productos que la compañia comercializa son los siguientes:

~~~
Código  | Producto    | Precio
-------------------------------
865123  | Mouse       | $15900
897162  | Teclado     | $8900
897123  | Impresoa    | $34900
~~~

El sistema debe registrar las ventas, solicitando el código del producto y la cantidad a comprar. El registro de compras lo debe realizar hasta que el usuario indique lo contrario. Ejemplo:

~~~
Ingrese código de producto: 897162
Ingrese la cantidad: 2

¿Desea agregar otro producto (1=si, 2=no)?
~~~

Una vez registrado la información de compra usted debe entregar el siguiente reporte de ventas:

~~~
Código  | Producto  | Ventas    | IVA (19%) |   sub-total
----------------------------------------------------------
865123  | Mouse     | $100000   | $19000    | $119000
897162  | Teclado   | $350000   | $66500    | $416500
897123  | Impresora | $532000   | $101080   | $633080
----------------------------------------------------------
TOTAL: $1168580
~~~

Donde:

* Producto: Indica el nombre del producto
* Ventas: Indica la suma total de todos los productos comprados
* IVA: Es el 19% de la suma total de todas las compras del producto
* Sub-total: Es la suma total de todas las compras del producto con iva incluido (suma de ventas + iva)
* TOTAL: Es la suma total de todas las ventas 


### Requisitos funcionales
* Debe validar que el código del producto exista a través de una función que debe implementar llamada ***EsProductoValido()***
* El código, el nombre del producto y los precios deben estar en tres vectores diferentes
* La información de los productos debe venir inicializada en el programa
* Los precios deben permitir decimales
* Las compras registradas son sin IVA
* El máximo de ventas que puede registrar serán 99 y lo debe validar con una función aparte para tal fin
* El reporte de ventas debe utilizar un vectores para su construcción, si se agregan más productos, el reporte se debería genera igual

### Condiciones de entrega
* Debe enviar el código fuente con el nombre examen.c
* Debe adjuntar el código fuente y enviar un correo al email mcantillana@portal53.cl 
* Asunto del correo: PROGRAMACION1-EXAMEN
* Fecha de entrega: 11 Junio 2020 a las 22:00 hrs
