# Pauta Examen (Parte teórica)

## ¿Que es un algoritmo? 
Es una serie de **pasos finitos** para resolver un problema determinado.

## En los paradigmas de programación, existen dos grandes enfoques. Indique cuales son estos enfoques

Los dos enfoques son:

* Paradigma imperativo: Algunos ejemplos de este paradigma son la programacion estructurada, orientación a objetvos, orientado a eventos
* Paradigma declarativo: Algunos ejemplos de este paradogma son la programación funcional y la programación lógica

## ¿Qué es una cadena de caracteres?

Es un conjunto de caracteres que forman palabras o frases.

## ¿cuál es la salida de la siguiente instrucción ? printf("%c", 64);

Se imprime la representación del caracter 64 en código **ascii**. El caracter es **@**

## ¿Cuál es el carácter que delimita el término de una cadena de caracteres?

EL caracter que indica el fin de una cadena es:
~~~
'\0'
~~~

## ¿Cual es la diferencia entre gets(str) y scanf('%s', &str)?

gets (función deprecada en compiladores nuevos de gcc) permite capturar cadenas con espacios. Básicamente captura todo la línea del buffer de entrada (stdin) hasta esperar un enter  (salto de carro). La función scanf("%s", .. ) no captura los espacios, por lo que al ingresar una palabra con espacios, solo captura hasta el espacio. Para corregir lo anterior es necesario incorporar una expresión regular que controle este comportamiento.

## ¿Que realiza esta función fn descrita a continuación?
~~~
int fn(char str[]) {
    int count = 0;
    while(str[count] != '\0') {
        count ++;
    }
    return count;
}
~~~

Cuenta la cantidad de caracteres de una cadena. Es similar al resultado que entrega **strlen** de la biblioteca **string.h**

## ¿Cómo podemos en copiar el contenido de un vector en otro vector?
Para copiar el contenido de un vector a otro, debemos hacerlo elemento a elemento. Un ejemplo de lo anterio:
~~~
...
int vector1 [4] = {1,2,3,4};
int vector2[4];
int i;

for(i=0;i<4;i++) {
    vector2[i] = vector1[i];
}
...
~~~

## ¿Cómo podemos comparar el contenido de un vector con el contenido de otro vector?
Para comparar el contenido de un vector a otro vector se debe realizar comparando elemento a elemento. Un ejemplo de lo anterior:
~~~
...
int vector1 [4] = {1,2,3,4};
int vector2[4] = {2,3,4,5};
int contador=0;
int i;

for(i=0;i<4;i++) {
    if (vector2[i] != vector1[i]) {
    	contador ++;
    }
}

if(!contador) {
	printf("Los vectores son iguales");
} else {
	printf("Los vectores NO son iguales");
}
...
~~~


## ¿Por que es incorrecto el siguiente código?

~~~
int numeros [3];
int n;

for(i=0;i<10;i++) {
    numeros[i] = i;
}
~~~

Es incorrecto por que recorremos hasta 10 cuando el vector está definido en 3. Además de ello i parece no estar definido.

## ¿Por que es buena práctica utilizar funciones al momento de desarrollar?
Es una buena práctica desarrollar utilizando funciones, por que permite desacoplar nuestro programa en pequeñas funcionalidades que serán capaces de resolver problemas acotados. 

## Nombre al menos dos características que debe considerar al momento de escribir una función en C
Algunas caracteristicas podrían ser:

* Que tengan bajo acoplamiento
* Que tengan Alta cohesión
* Que solucionen un solo problemas
* Nuestro código es más legible
* Permite reutilizar código
* Nuestro programas quedan más cortos
* Podemos crear programas con un vocabulario más cercano al dominio del problema



