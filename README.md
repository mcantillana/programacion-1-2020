# Material del curso

## Material
### Clase 1: 
* Presentación: [IntroduccionProgramacion.pdf](docs/IntroduccionProgramacion.pdf)
* Video: 
    * Intro : https://unab-programacion1.s3.amazonaws.com/2020/programacion1--clase-1--intro.mp4
    * Lenguaje Natural: https://unab-programacion1.s3.amazonaws.com/2020/programacion1--clase1--lenguaje-natural.mp4


### Clase 2: 
* Presentación: [Desarrollo de Algoritmos.pdf](docs/Desarrollo_de_Algoritmos.pdf)
* Video: 
    * Pseudo código: https://unab-programacion1.s3.amazonaws.com/2020/programacion1-clase2--pseudocodigo.mp4
* Material extra:

### Clase 3: 
* Presentación: [Desarrollo de Algoritmos - Parte II](docs/Desarrollo de Algoritmos - Clase 3.pdf)
* Video : 
    * Ejercicios Pseudocódigo: https://unab-programacion1.s3.amazonaws.com/2020/programacion1-clase3--ejercicios-pseudocodigo.mp4
    * Ejercicio diagrama de flujos: https://unab-programacion1.s3.amazonaws.com/2020/programacion1-clase3--ejercicios-diagramaflujo.mp4
* Material extra:
    * Raptor Software: [raptor2019.msi](docs/raptor2019.msi)

### Clase 4:
* Video clase: https://unab-programacion1.s3.amazonaws.com/2020/programacion1-clase4--resolucion-de-ejercicios.mp4
* Material Extra:
    * [Contadores y Acumuladores](docs/contadores_y_acumuladores.md)

### Clase 5: 
* Presetntación: [Introducción al lenguaje C](docs/introduccion-al-lenguaje-c-351337.pdf) | [versión online](https://slides.com/mcantillana/introduccion-al-lenguaje-c-351337/)
* Video de la clase: https://unab-programacion1.s3.amazonaws.com/2020/programacion1-clase5--introduccion-c.mp4

### Clase 6: 
* Presetntación: [Funciones en lenguaje C](docs/unab-funciones-en-c.pdf) | [versión online](https://slides.com/mcantillana/unab-funciones-en-c)
* Video de la clase: https://unab-programacion1.s3.amazonaws.com/2020/programacion1-clase7--funciones-en-c.mp4

### Clase 7:
* Presentación: [Vectores y Cadena de caracteres](docs/vectores-y-matrices-en-lenguage-c.pdf)| [versión online](https://slides.com/mcantillana/vectores-y-matrices-en-lenguage-c/#/)
* Video de la clase: https://unab-programacion1.s3.amazonaws.com/2020/programacion1-clase8--vectores-y-cadenas.mp4
* Ejercicios realizados en clase: [vectores.zip](https://unab-programacion1.s3.amazonaws.com/2020/vectores.zip)

## Guía de ejercicios
* [Guía de ejercicios Pseudocódigos y diagrama de flujo](guias/guia1.md)
* [Guía de ejercicios Programación en C](guias/guia2.md)