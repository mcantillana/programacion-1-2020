# TAREA
## Descripción actividad
Se pide realizar un programa que gestiona las notas de una clase de 10 alumnos. El programa debe permitir solicitar el nombre del alumno (debe aceptar espacios) y la nota promedio (la nota debe ser de 1 a 7). Con la información recolectada el programa debe entregar la siguiente información:

* El promedio de todas las notas.
* El promedio de las notas menores de 5.
* Mostrar el alumno que tiene la mejor nota.
* Mostrar el alumno que tiene la peor nota.

## Condiciones de entrega
* La tarea debe ser desarrollada en parejas (solo dos personas)
* Debe enviar el fichero .c del ejercicio al email mcantillana [at] portal53 [dot] cl 
* Asunto: PROGRAMACION-1-2020-TAREA
* Fecha de entrega: 04 Junio 2020 a las 23:59 hrs

## Consideraciones
* Se evaluará el uso de vectores de cadenas
* Se evaluará el uso de vectores
* Se evaluará el uso de estructuras de control
* Validación de ingreso de notas
* Uso correcto de definición de tipo de datos