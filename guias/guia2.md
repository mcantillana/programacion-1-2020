# Guía de ejercicios en C
Programación en lenguaje C

## Ejercicios

### Introductorios
1.- Desarrolle un algoritmo que permita leer dos valores distintos, determinar cual de los dos valores es el mayor y mostrar por pantalla.

2.- Desarrolle un algoritmo que permita leer un valor cualquiera N y escriba si dicho número es par o impar.

3.- Escriba un programa que pida dos números enteros y que calcule la división, indicando si la división es exacta o no.
Escriba un algoritmo para indicar el nivel de riesgo que tiene a partir de su edad con el COVID-19.

4.- Escriba un programa que determine si un carácter ingresado es letra, número o simbolo.

5.- Los tres lados a, b y c de un triángulo deben satisfacer la desigualdad triangular: cada uno de los lados no puede ser más largo que la suma de los otros dos.Escriba un programa que reciba como entrada los tres lados de un triángulo, e indique si acaso el triángulo es inválido o no.


### Ciclos

1.- Escriba un programa que imprima el mayor, el menor y el promedio de una serie de cinco números ingresados por el teclado.

2.- Diseñe un algoritmo que imprima la siguiente serie de fracciones para n terminos con signos alternos, donde n es ingresado por el usuario. 

Por ejemplo si n = 5, se debe mostrar en pantalla las siguientes fracciones

<pre>
1/2 – 3/4 + 5/6 – 7/8 + 9/10
</pre>

Debes tener en cuenta la posición del elemento y ademas el signo que le acompaña a cada fracción.

3.- Escriba un programa que pida al usuario dos números enteros, y luego entregue la suma de todos los números que están entre ellos. Por ejemplo, si los números son 1 y 7, debe entregar como resultado **2 + 3 + 4 + 5 + 6 = 20.**

4.- Escriba un programa que entregue todos los divisores del número entero ingresado

e.g
<pre>
Ingrese numero: 200
1 2 4 5 8 10 20 25 40 50 100 200
</pre>

5. Desarolle un programa para estimar el valor de π usando la siguiente suma infinita:


![alt text](img/img_formula_ejercico_3.png "Serie Pi")

La serie en terminos de sumatoria es
![alt text](img/37ad291358d3106556bd86b84c114685445cf901.svg "Serie Pi")

La entrada del programa debe ser un número entero n que indique cuántos términos de la suma se utilizará.

e.g
<pre>
n: 3
3.466666666666667
</pre>

<pre>
n: 1000
3.140592653839794
</pre>

### Funciones
1.- Escriba una función en C, que permita calcular el factorial de un número.

Hint: El factorial de n o n factorial se define en principio como el producto de todos los números enteros positivos desde 1 (es decir, los números naturales) hasta n.

Por ejemplo, 5 ! = 1 × 2 × 3 × 4 × 5 = 120.

2.- Escriba una función que permita determinar si una palabra es palindrome o no lo es. 

Hint: utilice ejercicio desarrollado en clases

3.- Desarrollo una función en lenguaje de programación en C que se encargue de encriptar un mensaje, para ello debe recibir una cadena de texto como parámetro de entrada, a esta cadena se sumará 2 al código ASCII del carácter. Además debe crear otra función que desencripte el mensaje anterior.

4.- Reescriba el ejercicio 5 de la parte de ciclos utilizando funciones.


### Vectores y Matrices
1.- Realice un programa que lea 10 números por teclado, y entregue el mayor valor, el menor valor y el promedio de los números.

2.- Realice un programa que lea una cadena y la muestre al revés.

3.- Realice un programa que lea una cadena y diga cuantas vocales hay.

4.- Realice un programa que lea una cadena y diga cuantas mayúsculas hay.

5.- Realice un programa que lea una cadena y la encripte sumando 3 al código ASCII de cada carácter. Mostrar por pantalla.

6.- Se pide realizar un programa que gestiona las notas de una clase de 10 alumnos. El programa debe permitir solicitar el nombre del alumno (debe aceptar espacios) y la nota promedio (la nota debe ser de 1 a 7). Con la información recolectada el programa debe entregar la siguiente información:

* El promedio de todas las notas.
* El promedio de las notas menores de 5.
* Mostrar el alumno que tiene la mejor nota.
* Mostrar el alumno que tiene la peor nota.

7.- sí como hay números palíndromos, también hay palabras palíndromas, que son las que no cambian al invertir el orden de sus letras.

Por ejemplo, «reconocer», «Neuquén» y «acurruca» son palíndromos.

Escriba un programa que reciba como entrada una palabra e indique si es palíndromo o no. Para simplificar, suponga que la palabra no tiene acentos y todas sus letras son minúsculas:

<pre>
Ejemplos
Ingrese palabra: sometemos
Es palindromo
</pre>

<pre>
Ingrese palabra: rascar
No es palindromo
</pre>
