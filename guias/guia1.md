
# Guía de ejercicios
Psegudo códigos y Diagrama de flujos

## Ejercicios (Parte I)


1. Imprima los números del 3 al 50.

~~~
INICIO

contador entero;
contador = 3;

HACER
    ESCRIBIR contador;
    contador = contador + 1;
MIENTRAS (contador < 51)

FIN
~~~


2. Imprima los números del 50 al 10 en orden decreciente.
3. Imprima los números pares entre 0 y 80.
4. Imprima la suma de los 4 primeros números, en donde el número de inicio debe ser ingresado por el usuario. [Solución utilizando raptor](raptor/Ejercicio4.rap)

~~~
INICIO

numero_ingreso entero;
contador_ciclo entero;
sumar entero;

ESCRIBIR "Ingrese numero: ";
LEER numero_ingreso; // el valor de inicio

contador_ciclo = 1;
sumar = 0;

HACER
    sumar = sumar + numero_ingreso;
    numero_ingreso = numero_ingreso + 1;
    contador_ciclo = contador_ciclo + 1;
MIENTRAS (contador_ciclo < 5)

ESCRIBIR sumar;

FIN

~~~

Traza

![alt text](img/img_traza_ejercico_4.png "Traza Ejercicio 4")

5. Imprima la suma de los números impares hasta el 50
6. Escriba un programa para sacar el promedio de un curso. El programa debe permitir ingresar 4 notas y entregar el promedio.
7. Escriba un programa que pida dos números enteros y que calcule la división, indicando si la división es exacta o no.

~~~
INICIO
numero1 entero;
numero2 entero;

SI (numero1 MOD numero2 == 0) entonces
    ESCRIBIR "La división es exacta"
SINO
    ESCRIBIR "La división NO es exacta"
FIN SI

FIN
~~~

8. Escriba un algoritmo para indicar el nivel de riesgo que tiene a partir de su edad con el COVID-19.

~~~
INICIO

edad entero;

ESCRIBIR "Ingrese su edad";
LEER edad;

SI (edad < 30) entonces
    ESCRIBIR "Tu nivel de riesgo de contagio es BAJO";
SINO

    SI (edad < 50) entonces
        ESCRIBIR "Tu nivel de riesgo de contagio es MEDIO";
    SINO
        ESCRIBIR "Tu nivel de riesgo de contagio es ALTO";
    FIN SI

FIN SI


FIN
~~~

9. Escriba un programa que imprima el mayor, el menor y el promedio de una serie de cinco números ingresados por el teclado. | [Solución utilizando raptor](raptor/Ejercicio9.rap)

10. Escriba un programa que determine si un caracter ingresado es letra (simbolo lo consideraremos una letra) o número.

11. Escriba un programa que entregue la edad del usuario a partir de su fecha de nacimiento

12. Permita introducir un numero por teclado e imprima por pantalla si el número ingresado es par o impar.


~~~
INICIO

numero entero;

ESCRIBIR "Ingresa un numero";
LEER numero;

SI (numero MOD 2 == 0) entonces
    ESCRIBIR "El número ingresado es PAR";
SINO
    ESCRIBIR "El número ingresado es IMPAR";

FINSI

FIN
~~~

13. Los tres lados a, b y c de un triángulo deben satisfacer la desigualdad triangular: cada uno de los lados no puede ser más largo que la suma de los otros dos.Escriba un programa que reciba como entrada los tres lados de un triángulo, e indique si acaso el triángulo es inválido o no.


La desigualdad triangular debe cumplir el siguiente criterio
<pre>
a + b > c
a + c > b
b + c > a
</pre>


~~~
INICIO

a entero;
b entero;
c entero;

ESCRIBIR "Ingrese a";
LEER a;

ESCRIBIR "Ingrese b";
LEER b;

ESCRIBIR "Ingrese c";
LEER c;

SI ( ((a + b) > c) & ((a + c) > b) & ((b + c) > a) ) entonces
    ESCRIBIR "El Triangulo cumple la desigualdad";
SINO
    ESCRIBIR "El Triangulo NO cumple la desigualdad";
FINSI

FIN
~~~


Considerar
<pre>
Tabla operador &

V & V => V
V & F => F
F & V => F
F & F => F

Tabla del operador || 

V || V => V 
V || F => V
F || V => V
F || F => F
</pre>

## Ejercicios (Parte II)

1. Escriba un programa que muestre la tabla de multiplicar del 1 al 10 del número ingresado por el usuario:
2. Escriba un programa que pida al usuario dos números enteros, y luego entregue la suma de todos los números que están entre ellos. Por ejemplo, si los números son 1 y 7, debe entregar como resultado **2 + 3 + 4 + 5 + 6 = 20.**

3. Escriba un programa que entregue todos los divisores del número entero ingresado

e.g
<pre>
Ingrese numero: 200
1 2 4 5 8 10 20 25 40 50 100 200
</pre>

4. Desarolle un programa para estimar el valor de π usando la siguiente suma infinita:


![alt text](img/img_formula_ejercico_3.png "Serie Pi")

La serie en terminos de sumatoria es
![alt text](img/37ad291358d3106556bd86b84c114685445cf901.svg "Serie Pi")

La entrada del programa debe ser un número entero n que indique cuántos términos de la suma se utilizará.

e.g
<pre>
n: 3
3.466666666666667
</pre>

<pre>
n: 1000
3.140592653839794
</pre>


* Solución

<pre>
((-1)^n)/(2*n+1)

n=0
(1)/(1) => 1

n=1
(-1)/(3) => -1/3

n=2
(1)/(5) => 1/5

n=3
(-1)/(7) => -1/7

n=4
(1)/(9) => 1/9
</pre>

~~~
INICIO

n entero;
total real;
numerador real;
denominador real;
acumulador real;
resultado real;
i entero; // contador

acumulador = 0;

ESCRIBIR "Ingrese el valor de n";
LEER n;

DESDE (i=0;i<n;i=i+1)
INICIO
    numerador = ((-1)^i);
    denominador = (2*i+1); 
    total = numerador / denominador;
    acumulador = acumulador + total;
FIN

resultado = 4 * acumulador;
ESCRIBIR resultado;

FIN
~~~

### Traza
![alt text](img/img_tabla_ejercio_parte2_4.png "Serie Pi")

 
5. Diseñe un algoritmo que imprima la siguiente serie de fracciones para n terminos con signos alternos, donde n es ingresado por el usuario. 

Por ejemplo si n = 5, se debe mostrar en pantalla las siguientes fracciones

<pre>
1/2 – 3/4 + 5/6 – 7/8 + 9/10
</pre>

Debes tener en cuenta la posición del elemento y ademas el signo que le acompaña a cada fracción.
<!--
https://www.diloentutospc.com/serie-de-fracciones-en-pseint/ 
6. http://progra.usm.cl/apunte/ejercicios/1/suma-fracciones.html


7. http://progra.usm.cl/apunte/ejercicios/1/collatz.html

8. http://progra.usm.cl/apunte/ejercicios/1/media-armonica.html

-->